download composer.php
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar create-project --prefer-dist yiisoft/yii2-app-advanced cisco
php composer.phar require thelfensdrfer/yii2-ssh-console
php composer.phar require --prefer-dist yiisoft/yii2-faker
set db credential in common file
db migration script run
console ./yii migrate

server {
    # "/var/www/cisco/cisco/frontend/web/";
    access_log  /var/log/nginx/cisco-access.log  main;
    error_log  /var/log/nginx/cisco-error.log  main;


    server_name  cisco.local;
    root   "/var/www/cisco/cisco/frontend/web/";
    set $yii_bootstrap "index.php";

    charset utf-8;

    location / {
        index  index.html $yii_bootstrap;
        try_files $uri $uri/ /$yii_bootstrap?$args;
    }

    location ~ ^/(protected|framework|themes/\w+/views) {
        deny  all;
    }

    #avoid processing of calls to unexisting static files by yii
    location ~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$ {
        try_files $uri =404;
    }

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    location ~ \.php {
        fastcgi_split_path_info  ^(.+\.php)(.*)$;

        #let yii catch the calls to unexising PHP files
        set $fsn /$yii_bootstrap;
        if (-f $document_root$fastcgi_script_name){
            set $fsn $fastcgi_script_name;
        }

        fastcgi_pass   127.0.0.1:9000;
        include fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fsn;

        #PATH_INFO and PATH_TRANSLATED can be omitted, but RFC 3875 specifies them for CGI
        fastcgi_param  PATH_INFO        $fastcgi_path_info;
        fastcgi_param  PATH_TRANSLATED  $document_root$fsn;
    }

    # prevent nginx from serving dotfiles (.htaccess, .svn, .git, etc.)
    location ~ /\. {
        deny all;
        access_log off;
        log_not_found off;
    }
}


/* mysql create table router property */


CREATE TABLE `router_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sap_id` varchar(18) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `hostname` varchar(14) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `loopback` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `mac_address` varchar(17) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


 ./yii dummy
 
 ./yii dummy/unique-record 5
 ./yii dummy/unique-record 10
 php composer.phar  require graze/telnet-client
 ./yii dummy/telnet-client
 ./yii dummy/unique-record 5
 ./yii dummy/telnet
 ./yii dummy/telnet wifitics.com 11211
 ./yii dummy/telnet localhost 11211 execute
 ./yii console/disk-usage
 ./yii dummy/disk-usage
 ./yii dummy/diskusage
 ./yii dummy/filelist
./yii dummy/cpuload-restart
 ./yii dummy/process-wise-load

curl -X GET \
  'http://cisco.loc/api/index?token=abc&user=amitbajaj' \
  -H 'cache-control: no-cache' \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -H 'postman-token: 04da95c8-7f2c-a1c0-c0e5-bf27d74326bd'