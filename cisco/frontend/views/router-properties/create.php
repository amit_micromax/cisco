<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RouterProperties */

$this->title = Yii::t('app', 'Create Router Properties');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Router Properties'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="router-properties-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
