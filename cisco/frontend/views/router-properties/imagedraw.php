<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\RouterPropertiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Image draw from Image magic');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="router-properties-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <img src="<?php echo $imagepath?>">
    

</div>
