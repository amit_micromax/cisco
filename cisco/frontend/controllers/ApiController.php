<?php

namespace frontend\controllers;

use Yii;
use common\models\RouterProperties;
use frontend\models\RouterPropertiesSearch;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RouterPropertiesController implements the CRUD actions for RouterProperties model.
 */
class ApiController extends ActiveController {

    public $modelClass = 'frontend\models\ApiRouterProperties';

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create' => ['POST'],
                    'update' => ['PUT', 'PATCH', 'POST'],
                    'delete' => ['DELETE'],
                    'view' => ['GET'],
                    'index' => ['GET'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \Yii::$app->response->statusCode = 200;
        $response =\Yii::$app->response;
        $request = Yii::$app->request;
        $token = $request->get('token', '');
        $user = $request->get('user', '');
        if (empty($token)) {
            \Yii::$app->response->statusCode = 401;
            $response->data = ['message' => 'Invalid token'];
        } else if (empty($user)) {
            \Yii::$app->response->statusCode = 401;
            $response->data = ['message' => 'Invalid user'];
        } else {
            $user = \common\models\User::findOne(["username" => $user, "verification_token" => $token]);
            if (empty($user)) {
                \Yii::$app->response->code = 403;
                $response->data = ['message' => 'Unauthorized'];
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all RouterProperties models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \frontend\models\ApiRouterProperties::find(),
        ]);
        return $dataProvider;
    }

    /**
     * Displays a single RouterProperties model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ApiRouterProperties model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateRouter() {
        $model = new \frontend\models\ApiRouterProperties();



        if (Yii::$app->request->post()) {
            $postarray = Yii::$app->request->post();
            $postarray['ApiRouterProperties']['created_by'] = (int) @Yii::$app->user->getId();
            $postarray['ApiRouterProperties']['updated_by'] = (int) @Yii::$app->user->getId();
            $postarray['ApiRouterProperties']['created_at'] = new \yii\db\Expression('NOW()');

            if ($model->load($postarray)) {
                if ($model->save()) {
                    return $this->redirect(['index']);
                }
            }
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing RouterProperties model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $postarray = Yii::$app->request->post();

            $postarray['RouterProperties']['updated_by'] = (int) @Yii::$app->user->getId();
            $postarray['RouterProperties']['updated_at'] = new \yii\db\Expression('NOW()');

            if ($model->load($postarray)) {
                if ($model->save()) {
                    return $this->redirect(['index']);
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RouterProperties model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        RouterProperties::updateAll(['status' => 2], ['id' => $id]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the RouterProperties model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RouterProperties the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = RouterProperties::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
