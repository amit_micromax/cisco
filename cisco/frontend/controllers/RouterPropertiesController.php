<?php

namespace frontend\controllers;

use Yii;
use common\models\RouterProperties;
use frontend\models\RouterPropertiesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RouterPropertiesController implements the CRUD actions for RouterProperties model.
 */
class RouterPropertiesController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RouterProperties models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new RouterPropertiesSearch();
        $param = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($param);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RouterProperties model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RouterProperties model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new RouterProperties();



        if (Yii::$app->request->post()) {
            $postarray = Yii::$app->request->post();
            $postarray['RouterProperties']['created_by'] = (int) @Yii::$app->user->getId();
            $postarray['RouterProperties']['updated_by'] = (int) @Yii::$app->user->getId();
            $postarray['RouterProperties']['created_at'] = new \yii\db\Expression('NOW()');

            if ($model->load($postarray)) {
                if ($model->save()) {
                    return $this->redirect(['index']);
                }
            }
        }
        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing RouterProperties model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $postarray = Yii::$app->request->post();

            $postarray['RouterProperties']['updated_by'] = (int) @Yii::$app->user->getId();
            $postarray['RouterProperties']['updated_at'] = new \yii\db\Expression('NOW()');

            if ($model->load($postarray)) {
                if ($model->save()) {
                    return $this->redirect(['index']);
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RouterProperties model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        RouterProperties::updateAll(['status' => 2], ['id' => $id]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the RouterProperties model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RouterProperties the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = RouterProperties::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**

     * @return image draw from the imagemagik
     * 
     */
    public function actionImageDraw() {
        // Create a new imagick object
        $imagick = new \Imagick();

// Create a image on imagick object
        $imagick->newImage(800, 250, 'white');

// Create a new ImagickDraw object
        $draw = new \ImagickDraw();

// Set the stroke color
        $draw->setStrokeColor('black');

// Set the fill color
        $draw->setFillColor('white');
// Create a new imagick object
        $imagick = new \Imagick();

// Create a image on imagick object
        $imagick->newImage(800, 250, 'white');

// Create a new ImagickDraw object
        $draw = new \ImagickDraw();

// Set the stroke color
        $draw->setStrokeColor('black');

// Set the fill color
        $draw->setFillColor('white');

// Set the stroke width
        $draw->setStrokeWidth(3);

// Set the font size
//$draw->setFontSize(32);
// Push  
        $draw->push();

// Translate the object
        $draw->translate(50, 50);

// Draw a circle
        $draw->setStrokeColor('gray');
        $draw->line(250, 30, 550, 30);
        $draw->line(255, 50, 300, 100);
        $draw->line(315, 110, 380, 150);
        $draw->line(380, 150, 475, 100);
        $draw->line(475, 100, 550, 50);

        $draw->setStrokeColor('black');
        $draw->circle(250, 30, 250, 50);
        $draw->circle(550, 30, 550, 50);

        $draw->setStrokeColor('gray');

        $draw->circle(300, 100, 300, 120);
        $draw->circle(475, 100, 475, 120);
        $draw->circle(380, 150, 380, 170);

// Pop because we want to draw a new
// circle with new properties.
        $draw->pop();


// Render the draw commands
        $imagick->drawImage($draw);

// Show the output
        $imagick->setImageFormat('png');
        $filename = time() . ".png";
        $imagick->getImageBlob();
        file_put_contents(Yii::$app->params['image']['image_path'] . $filename, $imagick); // works, or:
        //header("Content-Type: image/png");





        return $this->render('imagedraw', [
                    'imagepath' => Yii::$app->params['image']['image_url'] . $filename,
        ]);
    }

    /**
     * Lists from query provide.
     * $id is taken as query
     * @return mixed
     */
    public function actionQueryView($id = "select 1") {
       

        $dataProvider = new SqlDataProvider([
            'sql' => $id,
            'params' => [':status' => 1],
            'totalCount' => $count,
            'sort' => [
                'attributes' => [
                    'age',
                    'name' => [
                        'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
                        'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
                        'default' => SORT_DESC,
                        'label' => 'Name',
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
          return $this->render('queryview', [
                    
                    'dataProvider' => $dataProvider,
        ]);
    }

}
