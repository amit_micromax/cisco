<?php

return [
    'adminEmail' => 'admin@example.com',
    'image' => ['image_path' => dirname(__DIR__) . '/web/upload/', 'image_url' => '/upload/']
];
