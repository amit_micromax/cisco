<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "router_properties".
 *
 * @property int $id
 * @property string|null $sap_id
 * @property string|null $hostname
 * @property string|null $loopback
 * @property string|null $mac_address
 * @property int|null $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $update_at
 * @property int|null $updated_by
 */
class RouterProperties extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static $_is_status=['0'=>"active",'1'=>"in-active","2"=>"deleted"];
    public static function tableName()
    {
        return 'router_properties';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'update_at'], 'safe'],
            [['sap_id'], 'string', 'max' => 18],
            [['hostname'], 'string', 'max' => 14],
            [['loopback'], 'string', 'max' => 15],
            ['loopback', 'ip', 'ipv6' => false], // IPv4 address (IPv6 is disabled)
            [['mac_address'], 'string', 'max' => 17],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sap_id' => Yii::t('app', 'SAP ID'),
            'hostname' => Yii::t('app', 'Hostname'),
            'loopback' => Yii::t('app', 'Loopback'),
            'mac_address' => Yii::t('app', 'Mac Address'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'update_at' => Yii::t('app', 'Update At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
}
