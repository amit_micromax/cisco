<?php

namespace app\commands;

use \yii2sshconsole\Controller;

class ConsoleController extends Controller {

    public $defaultAction = 'exec';

    public function actionExec() {
        $this->auth('example.com', [
            'username' => 'myusername',
            'password' => 'mypassword', // optional
        ]);

        // Or via private key
        /*
          $this->auth('example.com', [
          'username' => 'myusername',
          'key' => '/path/to/private.key',
          'password' => 'mykeypassword', // optional
          ]);
         */


        $output = $this->run([
            'cd /path/to/install',
        ]);

        // Or via callback
        $this->run([
            'cd /path/to/install',
                ], function($line) {
            var_dump($line);
        });
    }

    public function actionSshByUser($host, $user, $key, $pass = '') {
        $param = [];
        $param['username'] = $user;
        $param['key'] = $key;
        if (!empty($pass))
            $param['pass'] = $pass;


        $this->auth($host, $param);

        $this->run([
            'cd /path/to/install',
                ], function($line) {
            var_dump($line);
        });
    }
    
    
    

    public function actionSshByKey($host, $user, $pass) {
        $this->auth('example.com', [
            'username' => 'myusername',
            'password' => 'mypassword', // optional
        ]);



        $this->run([
            'cd /path/to/install',
                ], function($line) {
            var_dump($line);
        });
    }


}
