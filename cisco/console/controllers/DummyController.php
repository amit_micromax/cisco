<?php

namespace console\controllers;

use Faker\Factory;
use Graze\TelnetClient\TelnetClient;
use Yii;
use yii\console\Controller;

class DummyController extends Controller {

    public $verbose = false;

    public function actionIndex() {
        echo "cron service runnning";
    }

    public function actionUniqueRecord($no) {
        for ($x = 0; $x <= 10; $x++) {
            $faker = Factory::create();
            $faker->sap_id = $sap_id = $faker->randomDigit();
            $hostname = $faker->text(14);
            $loopback = $faker->ipv4();
            $mac_address = $faker->macAddress();
            $status = $faker->randomDigit(0, 1);
            $created_at = $faker->date('Y-m-d H:i:s');
            $created_by = $faker->randomDigit();
            $update_at = $faker->date('Y-m-d H:i:s');
            $updated_by = $faker->randomDigit();
            Yii::$app->db->createCommand()->insert('router_properties', [
                'sap_id' => $sap_id,
                'hostname' => $hostname,
                'loopback' => $loopback,
                'mac_address' => $mac_address,
                'status' => $status,
                'created_at' => $created_at,
                'created_by' => $created_by,
                'update_at' => $update_at,
                'updated_by' => $updated_by,
            ])->execute();
        }
    }

    public function actionTelnet($host = '127.0.0.1', $port = '11211', $type = 'connect', $command = 'ls') {
        $client = TelnetClient::factory();
        $dsn = $host . ":" . $port;
        if ($type == 'connect') {
            $client->connect($dsn);
        } else if ($type == 'execute') {
            $client->connect($dsn);
            $resp = $client->execute($command);
            var_dump($resp);
        }
    }

    public function actionDiskusage() {
        exec("df 2>&1 &", $output);
        foreach ($output as $line) {
            echo "$line\n";
        }
    }

    public function actionFilelist() {
        exec("ls 2>&1 &", $output);
        foreach ($output as $line) {
            echo "$line\n";
        }
    }
    // return 
    public function actionCpuloadRestartServer() {
        $load = sys_getloadavg();
        echo "\ncpu load :" . $load['0'] . "\n";
        if ($load[0] > 0.80) {
            exec("/etc/init.d/httpd -k restart -c 2>&1 &", $output);
        }
    }

    public function actionFileCopy($type, $filecopy, $dstfile, $host, $user, $pass, $key) {
        if ($type = 'ftp') {
            if (copy('local/file.img', 'ftp://' . $user . ':' . $password . '@' . $host . $dstpath)) {
                echo "ftp file copied!!!";
            }
        }
    }
    
    public function actionLoad() {
        $return=null;
        if (is_readable("/proc/stat"))
        {
            $stats = @file_get_contents("/proc/stat");

            if ($stats !== false)
            {
                // Remove double spaces to make it easier to extract values with explode()
                $stats = preg_replace("/[[:blank:]]+/", " ", $stats);

                // Separate lines
                $stats = str_replace(array("\r\n", "\n\r", "\r"), "\n", $stats);
                $stats = explode("\n", $stats);

                // Separate values and find line for main CPU load
                foreach ($stats as $statLine)
                {
                    $statLineData = explode(" ", trim($statLine));

                    // Found!
                    if
                    (
                        (count($statLineData) >= 5) 
//                            /&& ($statLineData[0] == "cpu")
                    )
                    {
                        $return=array(
                            $statLineData,
                            $statLineData[2],
                            $statLineData[3],
                            $statLineData[4],
                        );
                    }
                }
            }
        }

        
        var_dump("load :", $return );
    }
    public function actionTop() {
        exec("iostat -c 2>&1 &", $output);
        foreach ($output as $line) {
            echo "$line\n";
        }
        exec("vmstat -d 2>&1 &", $output);
        foreach ($output as $line) {
            echo "$line\n";
        }
    }
    
    public function actionProcessWiseLoad(){
        
        exec("ps ahux --sort=-c | awk '{if($3>0.0)printf\"%s %6d %s\n\",$3,$2,$11}\'", $output);
        foreach ($output as $line) {
            echo "$line\n";
        }
        exec("vmstat -d 2>&1 &", $output);
        foreach ($output as $line) {
            echo "$line\n";
        }
        
    }


}
